
// angular app
    var app = angular.module('app', ['ui.router']);

    app.config(function($locationProvider, $stateProvider) {
        $locationProvider.hashPrefix('');
        $stateProvider
         .state('/', {
             url: '/',
             templateUrl: 'projects/projects.html',
             controller: 'homeCtrl'
         })
        .state('/about', {
             url: '/about',
            templateUrl: 'about.html',
            controller: 'aboutController'
        })
        .state('/ss', {
             url: '/ss',
            templateUrl: 'projects/ss.html',
            controller: 'ssController'
        })
        .state('/drp', {
             url: '/drp',
            templateUrl: 'projects/drp.html',
            controller: 'drpController'
        });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });

	app.run(['$rootScope', '$stateParams', function($rootScope, $stateParams){

        function titleToggle(){
            if ($("#title").is(".title-open")){
                $("#title").removeClass("title-open");
            }
            else{
                $("#title").addClass("title-open");
            }
        }

            function tickerToggle(){
                var tickerOnOff = function(){
                    $("#title").children().toggleClass("close");
                };
                if($("#title").is(".title-open")){
                    tickerOnOff();
                }
            }

                function tickerReplace(x){ // move this to app.js
                    titleToggle();
                    $("#ticker").html(x);
                }
                    
    // pageload
        function pageLoad(x){
            console.log(x.pageClass, "is online!");
            function containerToggle(){
                $("#main").toggleClass("container-open container-close");
            }
            setTimeout(titleToggle, 450);
                setTimeout(containerToggle, 1000);
                setTimeout(tickerToggle, 1750);
            setTimeout(titleToggle, 3500);
                    $("nav").on("click", function(){
                    $(this).toggleClass("nav-open");
                    tickerReplace("Cecil<br>Dunston");
                    console.log($(this));
            });
        }

    }]);

        app.controller('homeCtrl', function homeCtrl($rootScope) {
            pageClass = 'projects';
            pageLoad($rootScope);
            console.log(url);

        });

        function drpController($scope) {
            $scope.pageClass = 'drp';
            pageLoad($scope);
        }                           

        function aboutController($scope) {
            $scope.pageClass = 'about';
            pageLoad($scope);
        }

        function ssController($scope) {
            $scope.pageClass = 'ss';
            pageLoad($scope);
        }

        app.controller('drpController', drpController);
        app.controller('aboutController', aboutController);
        app.controller('ssController', ssController);