

// angular app
    var app = angular.module('app', ['ngRoute']);

    var url = "../data/work.json";

    app.config(function($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
         .when('/', {
             templateUrl: '../projects/projects.html',
             controller: 'homeController',
             controllerAs: 'home'
         })
        .when('/about', {
            templateUrl: 'about.html',
            controller: 'aboutController',
            controllerAs: 'about'
        })
        .when('/ss', {
            templateUrl: '../projects/ss.html',
            controller: 'ssController',
            controllerAs: 'ss'
        })
        .when('/drp', {
            templateUrl: '../projects/drp.html',
            controller: 'drpController',
            controllerAs: 'drp'
        })
        .otherwise({
            redirectTo: '/'
        });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });

// title and ticker

    function titleToggle(){
        if ($("#title").is(".title-open")){
            $("#title").removeClass("title-open");
        }
        else{
            $("#title").addClass("title-open");
        }
    }

        function tickerToggle(){
            var tickerOnOff = function(){
                $("#title").children().toggleClass("close");
            };
            if($("#title").is(".title-open")){
                tickerOnOff();
            }
        }

            function tickerReplace(x){ // move this to app.js
                titleToggle();
                $("#ticker").html(x);
            }
                
// pageload

    function pageLoad(x){
        console.log(x.pageClass, "is online!");
        function containerToggle(){
            $("#main").toggleClass("container-open container-close");
        }
        setTimeout(titleToggle, 450);
            setTimeout(containerToggle, 1000);
            setTimeout(tickerToggle, 1750);
        setTimeout(titleToggle, 3500);

    }
    
// nav


    function homeController($scope) {
        $scope.pageClass = 'projects';
        pageLoad($scope);

        function add_proj(i, $this, Class, Title, briefHeader, briefCopy){
            $("#projects").append("<a role='button' class='" + Class + "'><h3 class='proj-title'>" + Title + "</h3><div class='proj-details'><h2>" + briefHeader + "</h2><p>" + briefCopy + "</p></div></a>");  
            $("#projects").children().eq(i).children(".proj-details").append("<a href='/" + Class + "'>Explore</a>");    
        }

        function buttonTouch(i, $this, Class, Title, briefHeader, briefCopy){
            $("#projects").children().eq(i)
                 .on("touchstart", function(){
                     $(this).toggleClass("proj-hover");
                     $(this).siblings().toggleClass("proj-bg");
                     tickerReplace(Title);})
                 .on("touchend", function(){
                     titleToggle();
                     $(this).toggleClass("proj-hover");
                     $(this).siblings().toggleClass("proj-bg");
             });
        }

        function projBrief(){
            $("#projects").children().each(function(i){
                 if ($(this).is(".brief")) {
                     $(this).on("click", function(){
                          $(this).siblings().toggleClass("proj-hide");
                          $(this).toggleClass("brief");
                     });
                 }
                 else {      
                    $(this)
                         .on("click", function(){
                             titleToggle();
                             $(this).siblings().toggleClass("proj-hide");
                             $(this).toggleClass("brief");
                     });
                 }
            });
        }

        $.getJSON(url, function(data){
            $.each(data, function(i){
                $this = data[i];
                Class = $this.info.class;
                Title = $this.info.title;
                briefHeader = $this.content.site.brief.header;
                briefCopy = $this.content.site.brief.copy;
                add_proj(i, $this, Class, Title, briefHeader, briefCopy);
                buttonTouch(i, $this, Class, Title, briefHeader, briefCopy);
                console.log($this.info);
            });
            projBrief();

        });

    }

    function drpController($scope) {
        $scope.pageClass = 'drp';
        pageLoad($scope);
    }                           

    function aboutController($scope) {
        $scope.pageClass = 'about';
        pageLoad($scope);
    }

    function ssController($scope) {
        $scope.pageClass = 'ss';
        pageLoad($scope);
    }

    app.controller('homeController', homeController);
    app.controller('drpController', drpController);
    app.controller('aboutController', aboutController);
    app.controller('ssController', ssController);